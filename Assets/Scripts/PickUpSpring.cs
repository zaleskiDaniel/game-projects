﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpSpring : MonoBehaviour
{
    public delegate void GetBoost();
    public static event GetBoost OnGetBoost;

    private FadingText fadeText;
    [SerializeField] private AudioClip pickSpringAudio;

    private static bool boosted;

    private void Start()
    {
        fadeText = GameObject.Find("Canvas").transform.GetChild(4).GetComponent<FadingText>();
    }

    private void OnEnable()
    {
        Timer.OnTimerStop += AfterBoost;
    }
    private void OnDisable()
    {
        Timer.OnTimerStop -= AfterBoost;
    }

    private void AfterBoost()
    {
        boosted = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag =="Player")
        {
            if (boosted)
            {
                fadeText.SetText("You are under jump boost influence already", Color.red);            
                return;
            }
            if (OnGetBoost != null && !boosted)
            {
                boosted = true;
                AudioSource.PlayClipAtPoint(pickSpringAudio, transform.position);
                OnGetBoost();
                Destroy(gameObject);                
            }
             
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            fadeText.DisableText();
        }
        
    }


}
