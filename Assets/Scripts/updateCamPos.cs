﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class updateCamPos : MonoBehaviour
{
    [SerializeField] CameraController theCamera;
    [SerializeField] Vector3 position;
    [SerializeField] Vector3 rotation;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            theCamera.updateOffset(position);           
            theCamera.updateRotation(rotation);
        }
    }
}
