﻿using UnityEngine;
using UnityEngine.UI;

public class enemyHealthBar : MonoBehaviour
{
    [SerializeField] private float maxHealth;
    [SerializeField] private float damageModifier;
    [SerializeField] private Slider enemyHealthSlider;     
    [SerializeField] private AudioClip deathSound;
    [SerializeField] private bool dropItem;
    [SerializeField] private GameObject itemToDrop;    
    [SerializeField] private float burnDamage;
    [SerializeField] private float burnLength;
    [SerializeField] private float timeBtwBurnDamage;
    public bool inflammable;
    [SerializeField] private GameObject burnEffect;
    [SerializeField] private bool shooterEnemy;
    [SerializeField] public GameObject[] itemsToDrop;

    private float endBurn;
    private float dealBurnDamage;
    
    private AudioSource enemyDamageSource;
    private float currentHealth;
    private float damage;
    private bool isBurning;   

    Quaternion enemyHealthRotation;

    public static float killedEnemies;


    void Start()
    {
        enemyHealthRotation = enemyHealthSlider.transform.parent.rotation;
        currentHealth = maxHealth;
        enemyHealthSlider.maxValue = currentHealth;
        enemyHealthSlider.value = currentHealth;
        enemyDamageSource = GetComponent<AudioSource>();        
        
    }

    
    void Update()
    {       
        enemyHealthSlider.transform.parent.rotation = enemyHealthRotation;
       
        if (isBurning && Time.time > dealBurnDamage)
        {
            AddDamage(burnDamage);
            dealBurnDamage += timeBtwBurnDamage;            
        }
        if (Time.time > endBurn)
        {
            isBurning = false;
            burnEffect.SetActive(false);
        }

    }

    public void AddDamage(float damageValue)
    {
        enemyDamageSource.Play();
        enemyHealthSlider.gameObject.SetActive(true);
        damage = damageValue * damageModifier;
        if (damage <= 0) return;
        currentHealth -= damage;
        enemyHealthSlider.value = currentHealth;
        if (currentHealth <= 0) MakeDead();                
    }

    public void AddFire()
    {
        isBurning = true;
        burnEffect.SetActive(true);
        endBurn = Time.time + burnLength;
        dealBurnDamage = Time.time + timeBtwBurnDamage;
        
    }

    public void MakeDead()
    {
        if (transform.tag == "AreaEnemy") killedEnemies++;

        AudioSource.PlayClipAtPoint(deathSound, transform.position, 1f);

        if (shooterEnemy)
        {
            ShooterEnemyController theShooterEnemy = transform.root.GetComponent<ShooterEnemyController>();
            theShooterEnemy.MakeRagdoll();
        }
        else if(!shooterEnemy)
        {
            Enemy zombieController = transform.root.GetComponent<Enemy>();
            zombieController.MakeRagdoll();
        }
        

        Destroy(gameObject);

        if (dropItem)
        {
            GameObject item = itemsToDrop[Random.Range(0, itemsToDrop.Length)];
            Quaternion itemQuaternion = transform.rotation;
            itemQuaternion.y = 90f;
            Instantiate(item, transform.position + new Vector3(0, 1f, 0), item.transform.rotation);

        }
        
    }
    

}
