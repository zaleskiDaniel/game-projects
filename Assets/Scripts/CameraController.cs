﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Vector3 moveCamera;
    [SerializeField] private Transform player;
    Vector3 offset;
    Vector3 targetCameraPos;

    public static bool isFallen;

    private void Start()
    {       
        offset = transform.position - player.position;       
    }

    void Update()
    {
        CameraMove();
    }

    private void CameraMove()
    {
        if (!isFallen)
        {
            targetCameraPos = player.position + offset;
        }       
        transform.position = Vector3.Lerp(transform.position, targetCameraPos, 5F * Time.deltaTime);
    }

    public void updateOffset(Vector3 camPos)
    {
        offset = camPos - player.position;
    }

    public void updatePosition(Vector3 position)
    {
        transform.position = position;
    }

    public void updateRotation(Vector3 rotation)
    {
        transform.rotation = Quaternion.Euler(rotation);
    }




        
}
