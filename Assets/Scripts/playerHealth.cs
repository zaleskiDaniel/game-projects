﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerHealth : MonoBehaviour
{
    [SerializeField] private float maxHealth;
    [SerializeField] private Slider playerHealthSlider;
    [SerializeField] private Text playerHealthText;
    [SerializeField] private Image bloodScreenImage;
    [SerializeField] private Image defeatedImage;
    [SerializeField] private Image healedImage;
    [SerializeField] private float flashSpeed;
    private AudioSource playerAudioSource;
    
    

    private Image helpImage;
    private float currentHealth;
    private Color flashScreenColor = new Color(255f, 255f, 255f, 1f);
    private bool damaged = false;
    private bool healed;

    void Start()
    {
        helpImage = bloodScreenImage;
        currentHealth = maxHealth;
        playerHealthSlider.maxValue = maxHealth;
        playerHealthSlider.value = currentHealth;
        playerAudioSource = GetComponent<AudioSource>();
    }

    
    void Update()
    {
        UpdateHealthText(currentHealth, maxHealth);

        if (damaged)
        {
            helpImage = bloodScreenImage;
            helpImage.color = flashScreenColor;
            damaged = false;
        }
        else helpImage.color = Color.Lerp(bloodScreenImage.color, Color.clear, flashSpeed * Time.deltaTime);
    }

    public bool IsHealthMaximumAlready()
    {
        return currentHealth == maxHealth;
    }

    private void UpdateHealthText(float currentHealth, float maxHealth)
    {
        playerHealthText.text = currentHealth + "/" + maxHealth;
    }

    public void AddDamage(float damage)
    {
        damaged = true;
        currentHealth -= damage;
        playerHealthSlider.value -= damage;
        playerAudioSource.Play();
        if (currentHealth <= 0)
        {
            makeDead();            
        }
    }

    public void AddHealth(float health)
    {       
        currentHealth += health;
        if (currentHealth > maxHealth) currentHealth = maxHealth;         
        playerHealthSlider.value = currentHealth;
    }

    private void makeDead()
    {
        defeatedImage.GetComponent<Animator>().SetTrigger("EndGame");
        bloodScreenImage.color = flashScreenColor;
        Destroy(gameObject);
    }
}
