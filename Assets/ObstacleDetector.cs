﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDetector : MonoBehaviour
{
    private Enemy enemy;

    private void Start()
    {
        enemy = transform.root.GetComponent<Enemy>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Wall" || other.tag == "Ground")
        {
            enemy.Jump();
        }
    }
}
