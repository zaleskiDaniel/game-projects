﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroPlayerMovement : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] float speed;
    void Start()
    {
        rb = GetComponent<Rigidbody>();        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(Time.time);
        rb.MovePosition(transform.position + -speed * Vector3.forward * Time.deltaTime);
    }
}
