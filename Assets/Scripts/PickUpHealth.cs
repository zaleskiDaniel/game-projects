﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpHealth : MonoBehaviour
{
    [SerializeField] float healthAmount;
    [SerializeField] private AudioClip pickUpHearthSound;

    private FadingText fadeText;

    void Start()
    {
        fadeText = GameObject.Find("Canvas").transform.GetChild(4).GetComponent<FadingText>();
    }

    
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerHealth thePlayerHealth = other.GetComponent<playerHealth>();
            if (thePlayerHealth.IsHealthMaximumAlready())
            {
                fadeText.SetText("You're healthy, you don't need this", Color.green);
                return;
            }
            thePlayerHealth.AddHealth(healthAmount);                                   
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(pickUpHearthSound, transform.position, 1f);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            fadeText.DisableText();
        }
    }
}
