﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterEnemyController : MonoBehaviour
{
    [SerializeField] private Transform weaponPosition;
    [SerializeField] private Transform rightShoulderPos;
    [SerializeField] private float attackRange;
    [SerializeField] private float walkSpeed;
    [SerializeField] private float threatDistance;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject shootEffect;
    [SerializeField] private Transform firePoint;
    [SerializeField] private float fireRate;
    [SerializeField] private GameObject ragdoll;
    [SerializeField] private Transform playerPos;
    [SerializeField] private AudioClip shootClip;


    private Animator anim;
    private AudioSource audioSource;
    private Vector3 editedPlayerPos;

    private Rigidbody shooterRb;
    private float defaultX;
    private float defaultZ;
    private bool ableToShoot = true;

    private Vector3 targetLook;

    private bool flip;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        shooterRb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();            
    }


    void Update()
    {
        ShoulderHandler();
    }

    private void ShoulderHandler()
    {
        weaponPosition.position = rightShoulderPos.position;
        targetLook = Vector3.Lerp(targetLook, playerPos.position, 15f * Time.deltaTime);
        weaponPosition.LookAt(targetLook);
    }

    private void Move()
    {
        //moving
    }

    private void Flip()
    {
        Quaternion target = Quaternion.LookRotation(playerPos.position - transform.position);
        Quaternion lerpValue = transform.rotation;
        float defaultX = transform.rotation.x;
        float defaultZ = transform.rotation.z;
        lerpValue = Quaternion.Lerp(lerpValue, target, 8f * Time.deltaTime);
        lerpValue.x = defaultX;
        lerpValue.z = defaultZ;
        transform.rotation = lerpValue;        
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            Flip();

            float distance = Vector3.Distance(transform.position, playerPos.position);

            if (distance > attackRange)
            {
                shooterRb.MovePosition(transform.position + transform.forward * walkSpeed * Time.deltaTime);
            }
            if (distance <= attackRange)
            {
                if (ableToShoot)
                {
                    PlaySound(shootClip);
                    GameObject tmp = Instantiate(shootEffect, firePoint.position, firePoint.rotation);
                    Instantiate(bullet, firePoint.position, firePoint.rotation);
                    ParticleSystem shootEffectPS = tmp.transform.GetChild(0).GetComponent<ParticleSystem>();
                    Destroy(tmp, shootEffectPS.main.duration);
                    StartCoroutine(ShootCoroutine());
                }                
            }
        }      
    }

    private void PlaySound(AudioClip audio)
    {
        audioSource.clip = audio;
        audioSource.Play();
    }

    private IEnumerator ShootCoroutine()
    {
        ableToShoot = false;
        yield return new WaitForSeconds(fireRate);
        ableToShoot = true;
    }

    public void MakeRagdoll()
    {
        GameObject shooterRagdoll = Instantiate(ragdoll, transform.position, transform.rotation);

        Transform ragdollHips = shooterRagdoll.transform.Find("Hips");
        Transform zombieHips = transform.Find("Hips");

        Transform[] ragdollBones = ragdollHips.transform.GetComponentsInChildren<Transform>();
        Transform[] zombieBones = zombieHips.transform.GetComponentsInChildren<Transform>();

        for (int i = 0; i < ragdollBones.Length; i++)
        {
            for (int k = 0; k < zombieBones.Length; k++)
            {
                if (ragdollBones[i].name == zombieBones[k].name)
                {
                    ragdollBones[i].position = zombieBones[k].position;
                    ragdollBones[i].rotation = zombieBones[k].rotation;
                    
                }
            }
        }
    }
}
