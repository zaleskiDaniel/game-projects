﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAnimOnEnable : MonoBehaviour
{
    private Animator anim;

    private void OnEnable()
    {
        anim = GetComponent<Animator>();
        anim.Play("Enable");
    }
    private void OnDisable()
    {
        anim = GetComponent<Animator>();
        anim.Play("Disable");
    }
}
