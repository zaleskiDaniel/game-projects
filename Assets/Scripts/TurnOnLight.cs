﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnLight : MonoBehaviour
{
    private Animator lightAnim;


    void Start()
    {
        SpawnZombieController.OnEnemiesKilled += TurnOn;
        lightAnim = GetComponent<Animator>();
    }

    private void OnDisable()
    {
        SpawnZombieController.OnEnemiesKilled -= TurnOn;
    }

    private void TurnOn()
    {
        lightAnim.SetTrigger("TurnOnLight");
    }
}
