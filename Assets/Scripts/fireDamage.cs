﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireDamage : MonoBehaviour
{
    [SerializeField] private float damageValue;
    [SerializeField] private float timeBtwDamage;       

    private bool playerInRange;
    private bool enemyInRange;
    playerHealth thePlayerHealth;
    GameObject player;
    private bool dealDamage = true;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        thePlayerHealth = player.GetComponent<playerHealth>();
    }

    void Update()
    {
        if (playerInRange && dealDamage)
        {
            Attack();            
        }                   
    }

    void Attack()
    {        
        thePlayerHealth.AddDamage(damageValue);
        dealDamage = false;
        StartCoroutine(AttackCoroutine());
    }

    private IEnumerator AttackCoroutine()
    {
        yield return new WaitForSeconds(timeBtwDamage);
        dealDamage = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            playerInRange = true;
            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerInRange = false;
        }
    }
}
