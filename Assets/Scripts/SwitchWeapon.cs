﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchWeapon : MonoBehaviour
{

    [SerializeField] private GameObject pistolModel;
    [SerializeField] private GameObject rifleModel;
    [SerializeField] Image weaponImage;
    [SerializeField] Sprite rifle;
    [SerializeField] Sprite pistol;


    private WeaponIK weaponIK;
    private Player playerController;

    public bool PickedUpPistol;

    private Animator changeAnim;
    
    
    void Start()
    {
        changeAnim = weaponImage.GetComponent<Animator>();
        weaponIK = transform.root.GetComponent<WeaponIK>();
        playerController = transform.root.GetComponent<Player>();
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Alpha2) && PickedUpPistol)
        {
            SetGun(pistolModel);
            GunSwitch(rifleModel,false);
            GunSwitch(pistolModel, true);
            AnimationHandler(pistol);
            
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            AnimationHandler(rifle);
            SetGun(rifleModel);
            GunSwitch(pistolModel,false);
            GunSwitch(rifleModel, true);
            
        }

    }

    private void AnimationHandler(Sprite sprite)
    {
        weaponImage.sprite = sprite;
        changeAnim.SetTrigger("GetAnim");
        StartCoroutine(ChangeAnimTimer());
    }

    IEnumerator ChangeAnimTimer()
    {
        yield return new WaitForSeconds(1f);
        changeAnim.SetTrigger("GetAnim");
    }

    private void SetGun(GameObject gun)
    {
        gun.SetActive(true);
        weaponIK.rightHandTarget = gun.transform.GetChild(0).transform;
        weaponIK.leftHandTarget = gun.transform.GetChild(1).transform;
        playerController.riflePosition = gun.transform;
        weaponIK.pistolModel.position = gun.transform.GetChild(2).position;        
    }
    private void GunSwitch(GameObject gun, bool enable)
    {
        gun.gameObject.SetActive(enable);
        gun.transform.GetComponent<Shoot>().enabled = enable;
        gun.transform.GetComponent<AudioSource>().enabled = enable;
        gun.SetActive(enable);
    }
}

    

    


