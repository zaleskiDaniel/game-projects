﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnZombieController : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private ParticleSystem spawnEffect;
    [SerializeField] private float timeBtwSpawn;
    [SerializeField] private int maxEnemiesToSpawn;
    [SerializeField] private Text spawnText;
    [SerializeField] private GameObject chest;
    [SerializeField] private ParticleSystem chestParticles;

    private int randomZCoordinate;
    Vector3 randomVector;
    private bool ableToSpawn;
    private int spawnEnemyCounter;
    private int currentCountOfEnemies;
    private bool chestSpawned;

    public delegate void enemiesKilled();
    public static event enemiesKilled OnEnemiesKilled;

    void Start()
    {
        ableToSpawn = true;   
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            spawnText.gameObject.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            spawnText.gameObject.SetActive(false);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        
        if (other.tag == "Player")
        {
            currentCountOfEnemies = GameObject.FindGameObjectsWithTag("AreaEnemy").Length;
            spawnText.text = "Kill all enemies!" + Environment.NewLine + "Current count: " + enemyHealthBar.killedEnemies + "/" + maxEnemiesToSpawn;
            if (enemyHealthBar.killedEnemies == maxEnemiesToSpawn && !chestSpawned)
            {
                GameObject[] aliveEnemies = GameObject.FindGameObjectsWithTag("AreaEnemy");
                foreach (GameObject enemy in aliveEnemies)
                {
                    enemy.GetComponent<enemyHealthBar>().MakeDead();
                }

                if (OnEnemiesKilled != null)
                {
                    OnEnemiesKilled();
                }
                
                GameObject theChest =  Instantiate(chest);
                chestSpawned = true;
                ParticleSystem spawnParticles = Instantiate(chestParticles, theChest.transform.position, theChest.transform.rotation);              
                Destroy(spawnParticles, spawnParticles.main.duration);
            }
            SpawnEnemy();
          
        }
    }

    private void SpawnEnemy()
    {
        currentCountOfEnemies = GameObject.FindGameObjectsWithTag("AreaEnemy").Length;

        if (ableToSpawn && spawnEnemyCounter <= maxEnemiesToSpawn && currentCountOfEnemies <= 5 && enemyHealthBar.killedEnemies <= maxEnemiesToSpawn)
        {
            randomZCoordinate = UnityEngine.Random.Range(-110, -135);
            randomVector = new Vector3(-0.31f, -0.23f, randomZCoordinate);
            ParticleSystem theEffectOfSpawn = Instantiate(spawnEffect, randomVector, Quaternion.identity);
            GameObject zombie = Instantiate(enemy, randomVector, Quaternion.identity);           
            Destroy(theEffectOfSpawn, theEffectOfSpawn.main.duration);
            ableToSpawn = false;
            spawnEnemyCounter++;           
            StartCoroutine(SpawnTimer());
        }

        
        
    }

    private IEnumerator SpawnTimer()
    {
        yield return new WaitForSeconds(timeBtwSpawn);
        ableToSpawn = true;
    }
}
