﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallRespawn : MonoBehaviour
{
    [SerializeField] Vector3 spawnPositionAfterFallen;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            CameraController.isFallen = true;
            other.GetComponent<playerHealth>().AddDamage(25f);
            StartCoroutine(WaitTime(other.transform));
        }
    }

    IEnumerator WaitTime(Transform player)
    {        
        yield return new WaitForSeconds(1f);
        player.position = spawnPositionAfterFallen;
        CameraController.isFallen = false;
    }
}
