﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadingText : MonoBehaviour
{
    Text infoText;

    void Start()
    {
        infoText = GetComponent<Text>();
    }

    public void SetText(string text, Color color)
    {
        Color alphaZero = color;
        alphaZero.a = 0f;
        color = alphaZero;
        StartCoroutine(FadeInText(text, color));
    }

    public void DisableText()
    {
        StartCoroutine(FadeOutText());
    }

    private IEnumerator FadeOutText()
    {
        Color c = infoText.color;
        while (c.a >= 0.1f)
        {
            c.a -= 0.05f;
            infoText.color = c;
            yield return null;
        }
        c.a = 0f;
        infoText.color = c;
    }

    private IEnumerator FadeInText(string text, Color color)
    {
        infoText.color = color;
        infoText.text = text;
        Color c = infoText.color;
        while (c.a <= 0.9f)
        {
            c.a += 0.05f;
            infoText.color = c;
            yield return null;
        }
        c.a = 1f;
        infoText.color = c;
    }
}
