﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float crouchMoveSpeed;
    [SerializeField] private float backwardMoveSpeed;    
    [SerializeField] private float jumpForce;
    [SerializeField] private Animator animator;
    [SerializeField] private Transform rightShoulder;
    [SerializeField] private GameObject ceilingDetector;
    [SerializeField] private Transform slotForWeaponInHand;
    [SerializeField] private Camera backgroundCamera;
    [SerializeField] private LayerMask crouchLayerMask;
    [SerializeField] LayerMask bgMask;

    public GameObject bunnyEars;
    public bool isReloading;
    public Transform riflePosition;
    public Vector3 lookPosition;
    public bool ableToDoubleJump;

    private Rigidbody rb;
    private bool isGrounded;
    private float animValue;
    private bool horizontalFlip;       
    private float defaultMoveSpeed;
    private float currentMoveSpeed;
    private Ray mousePosition;
    private int jumpCount = 2;
    private bool doubleJump;
    private bool isCrouch;
    private bool inBackwardWalk;
    private bool backwardCrouchMove;
    public bool isCeilingOverhead;
    private CapsuleCollider idleCollider;    
    private Vector3 lerpLookPos;    
    private WeaponIK weaponIKScript;
    private Vector3 defaultPositionOfWeapon;
    private Vector3 defaultRotationOfWeapon;
    private bool jumpBoosted;


    private GameObject rightShoulderHelper;
    public static bool inEvent = false;

    void Start()
    {
        rightShoulderHelper = new GameObject("rightShoulderHelper");

        idleCollider = transform.GetComponent<CapsuleCollider>();
        rb = transform.GetComponent<Rigidbody>();        
        defaultMoveSpeed = moveSpeed;
        ableToDoubleJump = true;
        weaponIKScript = GetComponent<WeaponIK>();
        defaultPositionOfWeapon = riflePosition.GetChild(2).position;
        defaultRotationOfWeapon = riflePosition.GetChild(2).rotation.eulerAngles;
        
    }

    void Update()
    {
        AnimationHandler();
        if (!inEvent)
        {
            Move();
            Jump();
            HandleShoulder();
            Crouch();
        }
        
    }

    private void OnEnable()
    {
        PickUpSpring.OnGetBoost += MultipleJump;
        Timer.OnTimerStop += DisableMultipleJump;
    }

    private void OnDisable()
    {
        PickUpSpring.OnGetBoost -= MultipleJump;
        Timer.OnTimerStop -= DisableMultipleJump;
    }

    private void MultipleJump()
    {
        ableToDoubleJump = false;
        jumpForce *= 2f;                      
    }

    private void DisableMultipleJump()
    {
        ableToDoubleJump = true;
        jumpForce /= 2f;
    }

    private void Crouch()
    {
        CapsuleCollider playerCollider = transform.GetComponent<CapsuleCollider>();        
        isCeilingOverhead = Physics.Raycast(ceilingDetector.transform.position, Vector3.up,.7f,crouchLayerMask);

        if ((Input.GetKey(KeyCode.LeftControl)&&isGrounded) || isCeilingOverhead)
        {                       
            playerCollider.height = 1.4f;
            Vector3 playerColliderPosition = playerCollider.center;
            playerColliderPosition.y = .65f;
            playerCollider.center = playerColliderPosition;

            isCrouch = true;
            moveSpeed = crouchMoveSpeed;
        }
        else
        {
            playerCollider.height = 2.1f;
            Vector3 playerColliderPosition = playerCollider.center;
            playerColliderPosition.y = 1f;
            playerCollider.center = playerColliderPosition;
            isCrouch = false;
        }
    } 
   
    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space)&&isGrounded)
        {
            rb.AddForce(Vector3.up * jumpForce,ForceMode.Acceleration);
            isGrounded = false;
            jumpCount--;                 
        }
        else if (jumpCount > 0&& !isGrounded&& Input.GetKeyDown(KeyCode.Space) && ableToDoubleJump)
        {
            
            rb.velocity = Vector3.zero;
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Acceleration);
            jumpCount--;
            doubleJump = true;           
            StartCoroutine(DoubleJump());
            
            
        }
        
    }

    private IEnumerator DoubleJump()
    {
        yield return new WaitForSeconds(1f);        
        doubleJump = false;
    }

    public void ReloadAnimation()
    {    
       
        weaponIKScript.enabled = false;
        isReloading = true;
        //riflePosition.position = defaultPositionOfWeapon;
        animator.SetBool("Reload", true);    
        StartCoroutine(ReloadCoroutine());
        

    }

    private IEnumerator ReloadCoroutine()
    {
        yield return new WaitForSeconds(3f);        
        isReloading = false;
        animator.SetBool("Reload", false);
        weaponIKScript.enabled = true;

    }

    private void Move()
    {
        //Move       

        rb.MovePosition(transform.position + Input.GetAxisRaw("Horizontal") * -Vector3.forward * moveSpeed * Time.deltaTime);

        if (horizontalFlip && lookPosition.z < transform.position.z)
        {
            Flip();
        }
        else if (!horizontalFlip && lookPosition.z > transform.position.z)
        {
            Flip();
        }  

        //Backward Walk
        if ((lookPosition.z > transform.position.z && Input.GetAxisRaw("Horizontal") > 0 ||
            lookPosition.z < transform.position.z && Input.GetAxisRaw("Horizontal") < 0) && isGrounded)
        {
            if (isCrouch)
            {
                backwardCrouchMove = true;
                inBackwardWalk = false;
            }
            else
            {
                inBackwardWalk = true;
                moveSpeed = backwardMoveSpeed;
            }
            

        }
        else
        {
            backwardCrouchMove = false;
            inBackwardWalk = false;
            moveSpeed = defaultMoveSpeed;
        }
        
    }

    void HandleShoulder()
    {
        
        mousePosition = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        

        if (Physics.Raycast(mousePosition, out hit, Mathf.Infinity,bgMask))
        {
            Vector3 tmp = hit.point;         
            tmp.x = transform.position.x;
            lookPosition = tmp;
        }

        rightShoulderHelper.transform.parent = transform;
        rightShoulderHelper.transform.position = rightShoulder.position;
        

        riflePosition.position = rightShoulderHelper.transform.position;

        if (isReloading)
        {
            riflePosition.GetChild(2).eulerAngles = defaultRotationOfWeapon;
        }
        else
        {
            lerpLookPos = Vector3.Lerp(lerpLookPos, lookPosition, 18f * Time.deltaTime);

            riflePosition.LookAt(lerpLookPos);
        }
        
        
      
    }

    private void AnimationHandler()
    {
        if (Math.Abs(Input.GetAxisRaw("Horizontal")) > 0)
        {
            animValue = 1f;
        }
        else if (Input.GetAxisRaw("Horizontal") == 0)
        {
            animValue = 0;
        }                

        animator.SetBool("Jump", isGrounded);
        
        animator.SetBool("backwardWalk", inBackwardWalk);

        animator.SetFloat("Speed", animValue, .1f, Time.deltaTime);

        animator.SetBool("backwardCrouchWalk",backwardCrouchMove);

        animator.SetBool("DoubleJump", doubleJump);

        animator.SetBool("Crouch", isCrouch);

    }

    //private void Flip2()
    //{

    //    Quaternion targetRotation = Quaternion.LookRotation(lookPosition - transform.position);
    //    targetRotation.x = 0;
    //    targetRotation.z = 0;
    //    transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 18f);
    //}

    private void Flip()
    {
        horizontalFlip = !horizontalFlip;

        transform.Rotate(0, 180f, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Ground")
        {
            isGrounded = true;
            jumpCount = 2;          
        }
        if (collision.collider.tag == "Enemy")
        {
            GetComponent<playerHealth>().AddDamage(20f);
        }
    }
   


}
