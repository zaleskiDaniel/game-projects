﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{
    [SerializeField] Vector3 delta;
    [SerializeField] float speed;
    private Vector3 gizmosSize;
    Rigidbody rbPlatform;
    private Vector3 startPosition;
    private bool startMove = true;
    private float startTime;   

    Vector3 offset;
    Vector3 platformPos;

    void Start()
    {
        startMove = false;
        startPosition = transform.position;
        rbPlatform = GetComponent<Rigidbody>();
        startTime = Time.time;
    }


    void Update()
    {        
        float tOfLerp = ((Mathf.Sin((Time.time - startTime) * speed) + 1f) / 2f);
        platformPos = Vector3.Lerp(startPosition, startPosition + delta, tOfLerp);
        rbPlatform.MovePosition(platformPos);
    }

    private void OnDrawGizmos()
    {
        gizmosSize = transform.localScale;
        Gizmos.DrawWireCube(transform.position + delta, gizmosSize);
        Gizmos.color = Color.red;        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            other.transform.parent = transform;
        }
    }
} 