﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private GameObject destroyEffect;
    [SerializeField] private float bulletDamage;
    [SerializeField] bool isThatEnemyBullet;

    private void Update()
    {
        if (speed != 0)
        {
            transform.position += transform.forward * (speed * Time.deltaTime);
        }
        else
        {
            Debug.Log("No speed");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Range") Destroy(gameObject);                                    
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Enemy" || collision.collider.tag == "AreaEnemy")
        {
            GetDestroyEffect();
            enemyHealthBar theEnemyHealthBar = collision.gameObject.GetComponent<enemyHealthBar>();
            if (theEnemyHealthBar != null)
            {
                theEnemyHealthBar.AddDamage(bulletDamage);
            }
        }
        if (collision.collider.tag == "Wall" || collision.collider.tag == "Ground" || collision.collider.tag == "Bullet")
        {            
            GetDestroyEffect();
        }
        if (isThatEnemyBullet && collision.collider.tag == "Player")
        {
            GetDestroyEffect();
            collision.collider.GetComponent<playerHealth>().AddDamage(bulletDamage);
        }
        
    }
   


    private void GetDestroyEffect()
    {
        ParticleSystem destroyPS = destroyEffect.transform.GetChild(1).GetComponent<ParticleSystem>();
        Destroy(gameObject);
        GameObject tmpEffect = Instantiate(destroyEffect, transform.position, transform.rotation);
        Destroy(tmpEffect, destroyPS.main.duration);
    }
}
