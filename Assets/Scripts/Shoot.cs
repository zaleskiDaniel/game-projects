﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Shoot : MonoBehaviour
{
    [SerializeField] private GameObject bullet;    
    [SerializeField] private Transform firePoint;
    [SerializeField] private GameObject shootEffect;
    [SerializeField] private Slider playerAmmoSlider;
    [SerializeField] private Text playerAmmoText;
    [SerializeField] private float fireRate;
    [SerializeField] private float maxAmmo;
    [SerializeField] private float magazineSize;
    [SerializeField] private AudioClip shootClip;
    [SerializeField] private AudioClip reloadClip;
    [SerializeField] private AudioClip noAmmoClip;
    [SerializeField] private Text infoOnMiddle;
    [SerializeField] private Animator playerAnim;

    private Player playerScript;
    private AudioSource shootAudioSoruce;
    private float currentAmmo;
    private float currentStateOfMagazine;
    private bool ableToShoot = true;
    private float ammoMemory;

    private void Start()
    {       
        currentStateOfMagazine = magazineSize;
        playerAmmoSlider.maxValue = maxAmmo;
        currentAmmo = maxAmmo;
        playerAmmoSlider.value = currentAmmo;
        shootAudioSoruce = GetComponent<AudioSource>();
        playerScript = transform.root.GetComponent<Player>();
    }

    void Update()
    {        
        Shooting();
        AmmoText(currentAmmo, currentStateOfMagazine);
    }

    private void AmmoText(float currentAmmo, float magazineSize)
    {
        playerAmmoText.text = currentAmmo + "/" + magazineSize;
    }

    public bool IsMagazineFull()
    {
        return currentStateOfMagazine == magazineSize;
    }

    private void Shooting()
    {

        if (Input.GetButtonDown("Fire1") && ableToShoot && currentAmmo > 0 && !playerScript.isReloading && !Player.inEvent)
        {
            currentAmmo -= 1f;
            
            
            playerAmmoSlider.value = currentAmmo;
            GameObject tmp = Instantiate(shootEffect, firePoint.transform.position, firePoint.transform.rotation);
            Instantiate(bullet, firePoint.transform.position, firePoint.transform.rotation);

            PlaySound(shootClip);

            ableToShoot = false;
            StartCoroutine(ShootCoroutine());
            ParticleSystem psChild = tmp.transform.GetChild(0).GetComponent<ParticleSystem>();
            Destroy(tmp, psChild.main.duration);
        }
        else if (Input.GetKeyDown(KeyCode.R)&&currentStateOfMagazine > 0&&!(currentAmmo == maxAmmo))
        {
            playerScript.ReloadAnimation();
            infoOnMiddle.gameObject.SetActive(false);
            float reloadedCount = maxAmmo - currentAmmo;
            if (currentStateOfMagazine - reloadedCount >= 0)
            {                
                currentStateOfMagazine -= reloadedCount;                
                currentAmmo += reloadedCount;
                playerAmmoSlider.value = currentAmmo;
                PlaySound(reloadClip);


            }
            else if (currentStateOfMagazine > 0)
            {
                playerScript.ReloadAnimation();
                PlaySound(reloadClip);
                currentAmmo += currentStateOfMagazine;
                currentStateOfMagazine = 0f;
                playerAmmoSlider.value = currentAmmo;
            } 
            else Debug.Log("brak ammo");
        }
        else if (currentAmmo <= 0)
        {
            infoOnMiddle.color = Color.red;
            infoOnMiddle.text = "Press R to reload!";
            infoOnMiddle.gameObject.SetActive(true);
            if (Input.GetButtonDown("Fire1")) PlaySound(noAmmoClip);
        }
    }
    public void AddAmmo(float ammoValue)
    {
        currentStateOfMagazine += ammoValue;
        if (currentStateOfMagazine > magazineSize) currentStateOfMagazine = magazineSize;                
    }
    private void PlaySound(AudioClip clipToPlay)
    {
        shootAudioSoruce.clip = clipToPlay;
        shootAudioSoruce.Play();
    }
    private IEnumerator ShootCoroutine()
    {        
        yield return new WaitForSeconds(fireRate);
        ableToShoot = true;
    }
    private void OnEnable()
    {
        ableToShoot = true;
    }

   


}    