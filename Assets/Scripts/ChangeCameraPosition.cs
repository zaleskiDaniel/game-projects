﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraPosition : MonoBehaviour
{
    [SerializeField] Camera camera;
    [SerializeField] Vector3 position;
    [SerializeField] Vector3 rotation;
    [SerializeField] float fov;

    private IntroCamera theCamera;

    private void Start()
    {
        theCamera = camera.GetComponent<IntroCamera>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            theCamera.UpdatePosition(position);
            theCamera.UpdateRotation(rotation);
            theCamera.FOV(fov);
        }
    }
}
