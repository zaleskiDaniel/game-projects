﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpAmmo : MonoBehaviour
{
    [SerializeField] private float ammoValue;
    [SerializeField] private AudioClip getAmmoClip;
    [SerializeField] private FadingText infoText;

    void Start()
    {
        infoText = GameObject.Find("Canvas").transform.GetChild(4).GetComponent<FadingText>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Shoot theShoot = other.GetComponentInChildren<Shoot>();
            if (theShoot.IsMagazineFull())
            {
                infoText.SetText("Magazine is full!!", Color.yellow);
                return;
            }
            theShoot.AddAmmo(ammoValue);
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(getAmmoClip, transform.position,1f);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        infoText.DisableText();
    }


}
