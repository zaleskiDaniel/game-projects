﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CeilingDetector : MonoBehaviour
{
    private Enemy enemy;

    private void Start()
    {
        enemy = transform.root.GetComponent<Enemy>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Wall" || other.tag == "Ground")
        {
            enemy.crouch = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Wall" || other.tag == "Ground")
        {
            enemy.crouch = false;
        }
    }
}
