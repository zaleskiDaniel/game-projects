﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakePlayer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            Rigidbody player = collision.transform.GetComponent<Rigidbody>();
            player.MovePosition(player.transform.position +  Vector3.forward * GetComponent<Rigidbody>().velocity.z);
        }
    }
}
