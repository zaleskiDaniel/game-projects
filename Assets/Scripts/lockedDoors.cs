﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lockedDoors : MonoBehaviour
{
    public bool lockDoors;

    private HingeJoint leftDoor;
    private HingeJoint rightDoor;

    void Start()
    {
        lockDoors = true;
        leftDoor = transform.GetChild(1).GetComponent<HingeJoint>();
        rightDoor = transform.GetChild(2).GetComponent<HingeJoint>();
        SpawnZombieController.OnEnemiesKilled += UnlockDoors;       
    }

    // Update is called once per frame
    void Update()
    {
        if (!lockDoors)
        {
            ChangeLimits(leftDoor, -180f, 0);
            ChangeLimits(rightDoor, 180f, 0);
        }
        else
        {
            ChangeLimits(leftDoor, 0, 0);
            ChangeLimits(rightDoor, 0, 0);
        }
    }

    private void ChangeLimits(HingeJoint hingeJoint,float min, float max)
    {
        JointLimits theLimits = hingeJoint.limits;
        theLimits.min = min;
        theLimits.max = max;
        hingeJoint.limits = theLimits;
    }

    private void UnlockDoors()
    {
        lockDoors = false;
    }
}
