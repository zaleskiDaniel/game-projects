﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireBallBullet : MonoBehaviour
{     
    [SerializeField] private float speed;
    [SerializeField] private GameObject destroyEffect;

    void Update()
    {
        if (speed > 0)
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }
        else
        {
            Debug.Log("no speed");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Range") Destroy(gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Enemy" || collision.collider.tag == "AreaEnemy")
        {
            GetDestroyEffect();
            enemyHealthBar theEnemyHealthBar = collision.gameObject.GetComponent<enemyHealthBar>();
            theEnemyHealthBar.AddDamage(5);
            if (theEnemyHealthBar != null)
            {
                theEnemyHealthBar.AddFire();
            }
        }
        if (collision.collider.tag == "Wall" || collision.collider.tag == "Ground") GetDestroyEffect();       
    }

    private void GetDestroyEffect()
    {
        Destroy(gameObject);
        GameObject theEffect = Instantiate(destroyEffect, transform.position, transform.rotation) as GameObject;
        ParticleSystem destroyPS = theEffect.GetComponent<ParticleSystem>();
        Destroy(theEffect, destroyPS.main.duration);
    }
}
