﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float damage;
    [SerializeField] private float damageRate;
    [SerializeField] private float attackDistance;
    [SerializeField] private float walkSpeed;
    [SerializeField] private GameObject ragdollOfZombie;
    [SerializeField] private float jumpForce = 250f;

    private CapsuleCollider capsuleCollider;
    private CapsuleCollider standingCollider;
    private Rigidbody rbZombie;
    private Transform player;
    private bool dealDamage = true;    
    private Animator anim;
    private bool ableToAttack;
    private Vector3 startPosition;
    private bool isGrounded;
    public bool crouch;

    void Start()
    {
        capsuleCollider = GetComponent<CapsuleCollider>();
        standingCollider = capsuleCollider;
        rbZombie = GetComponent<Rigidbody>();
        player = GameObject.Find("Player").transform;
        anim = gameObject.GetComponent<Animator>();        
        startPosition = transform.position;        
    }
    
    void Update()
    {
        GetCrouch();
        AnimationHandler();
    }

    private void AnimationHandler()
    {
        anim.SetBool("Jump", isGrounded);
        anim.SetBool("Crouch", crouch);
        anim.SetBool("Attack", ableToAttack);
    }

    public void Jump()
    {
        rbZombie.AddForce(Vector3.up * jumpForce, ForceMode.Acceleration);
        isGrounded = false;
    }

    public void GetCrouch()
    {
        if (crouch)
        {
            capsuleCollider.height = 1.4f;
            Vector3 centerPosition = capsuleCollider.center;
            centerPosition.y = .65f;
            capsuleCollider.center = centerPosition;
        }
        else capsuleCollider = standingCollider;               
    }

    private void Attack()
    {
        ableToAttack = true;
        playerHealth playerHealth = player.GetComponent<playerHealth>();
        if (dealDamage)
        {
            playerHealth.AddDamage(damage);
            dealDamage = false;
            StartCoroutine(AttackTime());
        }

        
    }

    IEnumerator AttackTime()
    {
        yield return new WaitForSeconds(2.667f);
        ableToAttack = false;
        dealDamage = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "SpawnArea")
        {
            transform.gameObject.tag = "AreaEnemy";
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            Rotation(other);

            if (Vector3.Distance(transform.position, player.position) > attackDistance && !ableToAttack)
            {
                rbZombie.MovePosition(transform.position + transform.forward * walkSpeed * Time.deltaTime);                
                anim.SetFloat("Speed", 1f, .1f , Time.deltaTime);
            }
            else
            {                
                Attack();                
            }           
        }
    }

    private void Rotation(Collider other)
    {
        Quaternion targetRotation = Quaternion.LookRotation(other.transform.position - transform.position);

        float defaultX = transform.rotation.x;
        float defaultZ = transform.rotation.z;

        Quaternion enemyRotation = transform.rotation;

        enemyRotation = Quaternion.Slerp(transform.rotation, targetRotation, 18f * Time.deltaTime);

        enemyRotation.x = defaultX;
        enemyRotation.z = defaultZ;

        transform.rotation = enemyRotation;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            anim.SetFloat("Speed", 0f);
        }
    }

    public void MakeRagdoll()
    {
        GameObject zombieRagdoll = Instantiate(ragdollOfZombie, transform.root.position, Quaternion.identity) as GameObject;

        Transform ragdollZombieHips = zombieRagdoll.transform.Find("Hips");
        Transform currentZombieHips = transform.Find("Hips");

        Transform[] ragdollJoints = ragdollZombieHips.GetComponentsInChildren<Transform>();
        Transform[] zombieJoints = GetComponentsInChildren<Transform>();

        for (int i = 0; i < ragdollJoints.Length; i++)
        {
            for (int k = 0; k < zombieJoints.Length; k++)
            {
                if (ragdollJoints[i].name.CompareTo(zombieJoints[k].name) == 0)
                {
                    ragdollJoints[i].transform.position = zombieJoints[k].transform.position;
                    ragdollJoints[i].transform.rotation = zombieJoints[k].transform.rotation;
                    break;
                }
            }
        }
    }





}
