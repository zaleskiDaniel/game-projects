﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipColumn : MonoBehaviour
{
    [SerializeField] float force;
    Rigidbody rbColumn;  

    void Start()
    {
        
        rbColumn = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.collider.tag == "Player")
        {
            rbColumn.AddForce(force * Vector3.right *  (int)Random.Range(-1,2),ForceMode.Acceleration);
        }
    }
}
