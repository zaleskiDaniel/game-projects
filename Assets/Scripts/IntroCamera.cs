﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroCamera : MonoBehaviour
{    
    public void UpdatePosition(Vector3 position)
    {
        transform.position = position;
    }
    public void UpdateRotation(Vector3 rotation)
    {
        transform.rotation = Quaternion.Euler(rotation);
    }
    public void FOV(float fov)
    {
        transform.GetComponent<Camera>().fieldOfView = fov;
    }
}
