﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCrouchController : MonoBehaviour
{
    private Animator enemyAnim;
    private CapsuleCollider enemyCollider;
    private CapsuleCollider idleCollider;
   
    void Start()
    {
        enemyAnim = transform.root.GetComponent<Animator>();
        enemyCollider = transform.root.GetComponent<CapsuleCollider>();
        idleCollider = transform.root.GetComponent<CapsuleCollider>();
    }
    
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Wall")
        {
            EditCollider(1.4f, 0.65f);
            enemyAnim.SetBool("Crouch", true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Wall")
        {
            EditCollider(2.29f, 1.125f);
            enemyAnim.SetBool("Crouch", false);
        }
        
    }

    void EditCollider(float height, float yPos)
    {
        enemyCollider.height = height;
        Vector3 enemyColliderPosition = enemyCollider.center;
        enemyColliderPosition.y = yPos;
        enemyCollider.center = enemyColliderPosition;
    }

}
