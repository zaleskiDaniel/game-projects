﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenChest : MonoBehaviour
{
    
    [SerializeField] private GameObject itemIntoChest;
    [SerializeField] private ParticleSystem spawnItemEffect;

    private Text infoTextOnMiddle;
    private Animator chestAnim;

    void Start()
    {
        infoTextOnMiddle = GameObject.Find("Canvas").transform.GetChild(4).GetComponent<Text>();
        Instantiate(spawnItemEffect, transform.position, spawnItemEffect.transform.rotation);
        chestAnim = GetComponent<Animator>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            infoTextOnMiddle.color = Color.gray;
            infoTextOnMiddle.text = "Press E to open chest";
            infoTextOnMiddle.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                infoTextOnMiddle.gameObject.SetActive(false);
                chestAnim.SetTrigger("openChest");
                ParticleSystem particles = Instantiate(spawnItemEffect, itemIntoChest.transform.position, Quaternion.identity);
                itemIntoChest.gameObject.SetActive(true);
                Destroy(particles, particles.main.duration);
            }
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
        infoTextOnMiddle.gameObject.SetActive(false);
    }
}
