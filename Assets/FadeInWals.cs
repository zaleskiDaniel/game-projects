﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInWals : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Wall")
        {
            MeshRenderer mesh = other.GetComponent<MeshRenderer>();
            foreach (Material material in mesh.materials)
            {
                Color target = material.color;
                target.a = 0.1f;
                material.color = Color.Lerp(material.color, target, 10f * Time.deltaTime);
            }
        }                      
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Wall")
        {
            MeshRenderer mesh = other.GetComponent<MeshRenderer>();
            foreach (Material material in mesh.materials)
            {
                StartCoroutine(FadeOut(material));
            }
        }
              
    }

    private IEnumerator FadeOut(Material mat)
    {
        Color color = mat.color;
        while (color.a <= 0.9f)
        {
            color.a += 0.01f;
            mat.color = color;
            yield return null;
        }
    }

}
