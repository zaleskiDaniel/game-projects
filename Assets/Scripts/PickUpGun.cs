﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpGun : MonoBehaviour
{
    [SerializeField] private AudioClip pickUpClip;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.transform.Find("Arsenal").GetComponent<SwitchWeapon>().PickedUpPistol = true;
            AudioSource.PlayClipAtPoint(pickUpClip, transform.position);
            Destroy(gameObject);
        }
    }
}
