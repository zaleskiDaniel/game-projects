﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeInTitle : MonoBehaviour
{
    [SerializeField] Text text;
    
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {           
            StartCoroutine(FadeIn());
            StartCoroutine(StartLevel());
        }
    }

    IEnumerator FadeIn()
    {
        Color color = text.color;

        while (color.a <= 0.9f)
        {
            color.a += 0.008f;
            text.color = color;
            yield return null;

        }
    }

    IEnumerator StartLevel()
    {
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene("Level1");
    }
}
