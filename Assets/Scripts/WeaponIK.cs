﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponIK : MonoBehaviour
{
    [SerializeField] Transform target;
    public Transform rightHandTarget;
    public Transform slotForWeapon;
    public Transform pistolModel;
    public Transform leftHandTarget;
    public Vector3 offset1;
    public Vector3 offset2;
    public bool isThatPlayer;
    
    private Vector3 lookPosition;    
    private Animator anim;
    private Transform player;
    private Vector3 IKlookPos;

    Vector3 targetPosition;

    void Start()
    {
        anim = transform.GetComponent<Animator>();       
    }

    private void Update()
    {
        pistolModel.position = slotForWeapon.position;
        Quaternion offset = Quaternion.FromToRotation(offset1, offset2);
        pistolModel.rotation = slotForWeapon.rotation * offset;                       
    }

    

    private void OnAnimatorIK(int layerIndex)
    {       
        //LeftHand
        anim.SetIKPosition(AvatarIKGoal.LeftHand, leftHandTarget.position);
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
        anim.SetIKRotation(AvatarIKGoal.LeftHand, leftHandTarget.rotation);
        anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);

        //Right hand
        anim.SetIKRotation(AvatarIKGoal.RightHand, rightHandTarget.rotation);
        anim.SetIKRotationWeight(AvatarIKGoal.RightHand,1f);
        anim.SetIKPosition(AvatarIKGoal.RightHand, rightHandTarget.position);
        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
        if (isThatPlayer)
        {
            lookPosition = transform.GetComponent<Player>().lookPosition;
            IKlookPos = Vector3.Lerp(IKlookPos, lookPosition, 18f * Time.deltaTime);
            anim.SetLookAtPosition(IKlookPos);
        }
        if (!isThatPlayer)
        {                     
            targetPosition = Vector3.Lerp(targetPosition, target.position, 18f * Time.deltaTime);
            anim.SetLookAtPosition(targetPosition);           
        }        
        anim.SetLookAtWeight(1f, 1f, 1f, 1f, 1f);


    }

}
