﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootsActivator : MonoBehaviour
{
    [SerializeField] Camera shootCamera;

    private Animator cameraAnim;
    private bool shooted;

    void Start()
    {
        cameraAnim = shootCamera.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !shooted)
        {
            shooted = true;
            shootCamera.gameObject.SetActive(true);
            Player.inEvent = true;
            shootCamera.depth = 1f;
            cameraAnim.SetTrigger("camShoot");
            StartCoroutine(Deactivate());
        }
    }

    private IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(3.5f);
        shootCamera.gameObject.SetActive(false);
        Player.inEvent = false;
        cameraAnim.SetTrigger("camShoot");
        shootCamera.depth = -2f;
    }
}
