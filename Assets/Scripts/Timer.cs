﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{  
    private Text timer;
    private bool start;
    private float estimatedTime;
    private float startTime;
    private float time;

    
    private Color color;
    public delegate void TimerStop();
    public static TimerStop OnTimerStop;

    private void OnEnable()
    {
        gameObject.SetActive(true);
        PickUpSpring.OnGetBoost += TurnOnTimer;
    }

    private void OnDisable()
    {
        PickUpSpring.OnGetBoost -= TurnOnTimer;
    }

    void Start()
    {
        color = new Color(255f, 252f, 0f,255f);
        timer = GetComponent<Text>();
        timer.color = Color.clear;
    }

    public void TurnOnTimer()
    {
        
        timer.color = color;
        estimatedTime = 5f;
        startTime = Time.timeSinceLevelLoad;
        start = true;        
    }
    
    void Update()
    {
        if (start)
        {
            
            time = estimatedTime - Time.time + startTime;
            timer.text = "Timer" + Environment.NewLine + time.ToString("f02");

            if (time <= 0)
            {
                if (OnTimerStop != null)
                {
                    OnTimerStop();
                }
                timer.color = Color.clear;
                start = false;
            }
           
        }        
    }  
    
}
