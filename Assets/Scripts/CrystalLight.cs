﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class CrystalLight : MonoBehaviour
{
    private Light crystalPointLight;
    private float startIntensity;
    private ParticleSystem ps;

    void Start()
    {
        ps = GetComponentInChildren<ParticleSystem>();
        crystalPointLight = GetComponentInChildren<Light>();
        startIntensity = crystalPointLight.intensity;
    }

    
    void Update()
    {
        crystalPointLight.intensity = (Mathf.Sin(Time.time * 2.5f) + 1)/2 * startIntensity;
    }
}
