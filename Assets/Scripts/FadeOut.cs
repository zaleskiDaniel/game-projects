﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : MonoBehaviour
{
    [SerializeField] Material[] colors;

    private MeshRenderer meshRenderer;
    private bool isFaded;
    private Color defaultColor;
    private playerHealth player;

    void Start()
    {
        player = GameObject.Find("Player").GetComponent<playerHealth>();
        meshRenderer = GetComponent<MeshRenderer>();
        defaultColor = meshRenderer.material.color;
    }

    private void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.collider.tag == "Player")
        {
            if (meshRenderer.material.color == Color.red) player.GetComponent<playerHealth>().AddDamage(40f);          
            else if(meshRenderer.material.color == Color.green) player.GetComponent<playerHealth>().AddHealth(20f);
            StartCoroutine(Fading());
        }
    }

    IEnumerator Fading()
    {   
        while (defaultColor.a >= 0.01f)
        {
            defaultColor.a -= 0.05f;
            meshRenderer.material.color = defaultColor;
            yield return null;
        }
        gameObject.GetComponent<BoxCollider>().enabled = false;
        meshRenderer.enabled = false;
        StartCoroutine(FadeIn());       
    }

    IEnumerator FadeIn()
    {
        yield return new WaitForSeconds(2f);
        defaultColor = colors[Random.Range(0, colors.Length)].color;
        meshRenderer.enabled = true;        
        while (defaultColor.a <= 0.99f)
        {
            defaultColor.a += 0.05f;
            meshRenderer.material.color = defaultColor;
            yield return null;
        }
        gameObject.GetComponent<BoxCollider>().enabled = true;
        
    }

}
